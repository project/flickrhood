<?php

/* Implementation of hook_menu. */
function flickrhood_menu($may_cache) {
  global $user;
  
  if ($may_cache) {
  	//settings page
  	$items[] = array(
  		'path' => 'admin/settings/flickrhood',
  		'title' => 'Flickrhood',
  		'description' => 'Edit flickrhood options (some optional, some required)',
  		'callback' => 'drupal_get_form',
  		'callback arguments' => 'flickrhood_admin_settings',
  		'access' => user_access('administer site configuration'),
  		'type' => MENU_NORMAL_ITEM,
  	);
  }
  else {
  
  $items = array();
  $admin_access = user_access('administer flickr');
  $view_access = user_access('view all photos');
  
  if (arg(0) == 'flickrhood' && is_numeric(arg(1)) && arg(1) > 0) {
      $account = user_load(array('uid' => arg(1)));
      if ($account !== FALSE) {
        // Always let a user view their own account
        $view_access |= user_access('view own photos') && ($user->uid == arg(1));
        // Only admins can view blocked accounts
        $view_access &= $account->status || $admin_access;
		
		//main flickrhood user page(photos)
        $items[] = array(
			'path' => 'flickrhood/'. arg(1),
			'title' => $account->name . t("'s flickr"),
          	'type' => MENU_CALLBACK, 
			'callback' => 'flickrhood_viewuserphotos',
          	'callback arguments' => array(arg(1)),
			'access' => $view_access,
		);
		$items[] = array(
			'path' => 'flickrhood/'. arg(1) .'/view',
			'title' => t('photos'),
         	'type' => MENU_DEFAULT_LOCAL_TASK,
			'weight' => -10,
			'access' => $view_access,
		);
		//flickrhood user set page
		$items[] = array(
			'path' => 'flickrhood/'. arg(1) .'/sets',
			'title' => t("sets"),
        	'type' => MENU_LOCAL_TASK, 
			'callback' => 'flickrhood_viewusersetlist',
          	'callback arguments' => array(arg(1)),
			'access' => $view_access,
		);
		$items[] = array(
			'path' => 'flickrhood/'. arg(1) .'/sets/list',
			'title' => t("list"),
        	'type' => MENU_DEFAULT_LOCAL_TASK, 
			'access' => $view_access,
		);
		if (arg(3)!=NULL && arg(2)=='sets') {
			$set_info = _flickrhood_get_setinfo(arg(3));
			if (!$set_info || (_flickrhood_get_fid(arg(1)) != $set_info['owner'])) {
				drupal_goto('flickrhood/' . arg(1) . '/sets');
			}
			$items[] = array(
				'path' => 'flickrhood/'. arg(1) .'/sets/' . arg(3),
				'title' => t("set: ") . $set_info['title'],
	    		'type' => MENU_LOCAL_TASK,
				'callback' => 'flickrhood_viewuserset',
	    		'callback arguments' => array(arg(1), $set_info),
				'access' => $view_access,
			);
		}
		//flickrhood main tags page(cloud)
		$items[] = array(
			'path' => 'flickrhood/'. arg(1) .'/tags',
			'title' => t("tags"),
        	'type' => MENU_LOCAL_TASK, 
			'callback' => 'flickrhood_viewusertagcloud',
        	'callback arguments' => array(arg(1)),
			'access' => $view_access,
		);
		$items[] = array(
			'path' => 'flickrhood/'. arg(1) .'/tags/cloud',
			'title' => t("cloud"),
        	'type' => MENU_DEFAULT_LOCAL_TASK, 
			'access' => $view_access,
		);
		//flickrhood tag list page
		$items[] = array(
			'path' => 'flickrhood/'. arg(1) .'/tags/list',
			'title' => t("list"),
        	'type' => MENU_LOCAL_TASK, 
			'callback' => 'flickrhood_viewusertaglist',
        	'callback arguments' => array(arg(1)),
			'access' => $view_access,
		);
		//flickrhood specific tag page
		if ( (arg(3)!==NULL) && (arg(3)!='cloud') && (arg(3)!='list') ){
	        $items[] = array(
				'path' => 'flickrhood/'. arg(1) . '/tags/' . arg(3), 
				'title' => t('tags: ') . str_replace(',', ', ', arg(3)),
				'type' => MENU_LOCAL_TASK,
				'callback' => 'flickrhood_viewuserphotosbytags',
				'callback arguments' => array(arg(1), arg(3)),
				'access' => $view_access,
			);
		}
      }
      }
  }
  return $items;
}

/* Implementation of hook_perm */
function flickrhood_perm () {
	return array('view own photos', 'view all photos', 'view own tags', 'view all tags', 'administer flickr');
}

/* Implementation of hook_user */
function flickrhood_user ($op, &$edit, &$account, $category = NULL) { 
	if ($op == 'form' && $category == 'account') {
		//create defaults
		$user = user_load(array('uid'=>$account->uid));
		
		$form['flickr_settings'] = array(
    		'#type' => 'fieldset',
   			'#title' => t('FlickrHood User Settings'),
   			'#tree' => TRUE,
   			'#collapsible' => TRUE,
    		'#collapsed' => FALSE,
    		'#description' => t('You must enter either your Flickr username or email to have your photos recognized by FlickrHood'),
  			'#weight' => 2,
  		);
		$form['flickr_settings']['username'] = array(
			'#type' => 'textfield',
			'#title' => t('Flickr Username'),
			'#size' => 40,
			'#maxlength' => 40,
			'#description' => t('The username of your Flickr account'),
			'#default_value' => $user->flickrusername,
		);
		$form['flickr_settings']['mail'] = array(
			'#type' => 'textfield',
			'#title' => t('Flickr Email'),
			'#size' => 40,
			'#maxlength' => 40,
			'#description' => t('The email used in your Flickr account'),
			'#default_value' => $user->flickrmail,
		);
		return $form;
	}
	elseif ($op=='insert') {
		if (!empty($edit['flickr_settings']['mail']) || !empty($edit['flickr_settings']['username'])) {
			$f_user_array = array(
				'username' => $edit['flickr_settings']['username'] ? $edit['flickr_settings']['username'] : '',
				'mail' => $edit['flickr_settings']['mail'] ? $edit['flickr_settings']['mail'] : ''
			);
			
			$f_user = _flickrhood_find_user($f_user_array);
			if ($f_user!==FALSE) {
				$fid = $f_user['uid'];
				db_query('INSERT INTO {flickrhood_users} (uid,fid,mail,username) VALUES (%d,\'%s\',\'%s\',\'%s\')',
						$account->uid,$fid,$account->flickrmail,$account->flickrusername);
				_flickrhood_update_user_tags($account->uid);
			}
			else {
				drupal_set_message('Couldn\'t find a flickr user with the information given, the flickr functionality for your account will not be enabled.', 'error');
			}
		}
		unset($account->flickrusername);unset($account->flickrmail);unset($account->flickrid);
	}
	elseif ($op=='update') {
		
		if (!empty($edit['flickr_settings']['mail']) || !empty($edit['flickr_settings']['username'])) {
			$f_user_array = array(
				'username' => $edit['flickr_settings']['username'] ? $edit['flickr_settings']['username'] : '',
				'mail' => $edit['flickr_settings']['mail'] ? $edit['flickr_settings']['mail'] : ''
			);
			
			$f_user = _flickrhood_find_user($f_user_array);
			if ($f_user!==FALSE) {
				$fid = $f_user['uid'];
				//find if the user is already in the table
				$exists = db_query('SELECT uid FROM {flickrhood_users} WHERE uid=%d', $account->uid);
				//insert or update as neeeded
				if (db_num_rows($exists) <= 0) {
					db_query('INSERT INTO {flickrhood_users} (uid,fid,mail,username) VALUES (%d,\'%s\',\'%s\',\'%s\')',
						$account->uid,$fid,$f_user_array['mail'],$f_user_array['username']);
				}
				else {
					db_query('UPDATE {flickrhood_users} SET mail=\'%s\', username=\'%s\', fid=\'%s\' WHERE uid=\'%d\'',
		 			$f_user_array['mail'], $f_user_array['username'], $fid, $account->uid);
				}
				_flickrhood_update_user_tags($account->uid);
			}
			else {
				drupal_set_message('Couldn\'t find a flickr user with the information given, the flickr information you have changed will not be updated', 'error');
			}
		}
		else {
			//find if the user is already in the table
			$exists = db_query('SELECT uid FROM {flickrhood_users} WHERE uid=%d', $account->uid);
			if (db_num_rows($exists) > 0) {
				//the user is attempting to delete their flickrhood account
				db_query('DELETE FROM {flickrhood_users} WHERE uid=%d', $account->uid);
				db_query('DELETE FROM {flickrhood_users_tags} WHERE uid=%d', $account->uid);
			}
		}
		unset($account->flickrusername);unset($account->flickrmail);unset($account->flickrid);
	}
	elseif ($op=='load') {
		$result = db_query('SELECT username, mail, fid FROM {flickrhood_users} WHERE uid=\'%d\'', $account->uid);
		$fu_object = db_fetch_object($result);
		$account->flickrmail = $fu_object->mail;
		$account->flickrusername = $fu_object->username;
		$account->flickrid = $fu_object->fid;
	}
	elseif ($op=='delete') {
		db_query('DELETE FROM {flickrhood_users} WHERE uid=%d', $account->uid);
		db_query('DELETE FROM {flickrhood_users_tags} WHERE uid=%d', $account->uid);
	}
}

/* views a users photos and sets*/
function flickrhood_viewuserphotos($uid) {
	
	//set some required pager stuff
	drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);
	global $pager_page_array, $pager_total,$pager_total_items;
	//set this to something else if you want multiple pagers
	$element = 0;
  	$pager_page_array[$element] = $_GET['page'] ? $_GET['page'] : '';
	
	$per_page = variable_get('flickrhood_photos_per_page', 20);
	
	$fp_arr = _flickrhood_public_photos($uid, array(), $pager_page_array[$element]+1, $per_page);
	
	if ($fp_arr == FALSE) {
	    return '';
	}
	
	//set pager information we just acquired
	$pager_total[$element] = $fp_arr['pages'];
	$pager_total_items[$element] = $fp_arr['total'];
	
	return theme('flickrhood_user_photos', $uid, $fp_arr);
  	
}

function flickrhood_viewusertagcloud($uid) {
	
	//get users fid
	$fid = _flickrhood_get_fid($uid);
	
	if ($fid===FALSE) {
		// TODO watchdog error here
		drupal_set_message('Problem getting users tags', 'error');
		return '';
	}
	
	//get php flickr object
	$pfo = _flickrhood_get_pfo();
	
	//get tag info
	$poptag_response = $pfo->tags_getListUserPopular($fid, variable_get('flickrhood_tags_in_cloud','150'));
	
	if (!$poptag_response) {
		drupal_set_message('Problem retrieving user\'s tags', 'error');
		return FALSE;
	}
	elseif (empty($poptag_response)) {
		drupal_set_message('User has no tags');
		return '';
	}
	
	$tag_arr = array();
	foreach ($poptag_response as $tag) {
		$tag_arr[$tag['_content']] = $tag['count'];
	}
	
	if (count($tag_arr) < 1) {
		drupal_set_message('This user has no tags');
		return '';
	}

	//pass to theme function
	return theme('flickrhood_user_tagcloud', $uid, $fid, $tag_arr);
}

function flickrhood_viewusertaglist($uid) {
	//get users fid
	$fid = _flickrhood_get_fid($uid);
	
	if ($fid===FALSE) {
		// TODO watchdog error here
		drupal_set_message('Problem getting users tags', 'error');
		return '';
	}
	
	//get all tags
	$pfo = _flickrhood_get_pfo();
	$taglist_response = $pfo->tags_getListUser($fid);
	
	if (!$taglist_response) {
		drupal_set_message('Problem retrieving user\'s tags', 'error');
		return FALSE;
	}
	elseif (empty($taglist_response)) {
		drupal_set_message('User has no tags');
		return '';
	}
	
	//pass array to the theme function
	return theme('flickrhood_user_taglist', $uid, $fid, $taglist_response);
}

function flickrhood_viewusersetlist($uid) {
	//get users fid
	$fid = _flickrhood_get_fid($uid);
	
	if ($fid===FALSE) {
		// TODO watchdog error here
		drupal_set_message('Problem getting user\'s sets', 'error');
		return '';
	}
	
	$pfo = _flickrhood_get_pfo();
	$set_response = $pfo->photosets_getList($fid);
	
	if (!$set_response) {
		drupal_set_message('Problem retrieving user\'s sets', 'error');
		return FALSE;
	}
	//TODO 0 set case
	
	return theme('flickrhood_user_setlist', $uid, $fid, $set_response['photoset']);
}

function flickrhood_viewuserset($uid, $set_arr) {
	
	//set some required pager stuff
	drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);
	global $pager_page_array, $pager_total,$pager_total_items;
	//set this to something else if you want multiple pagers
	$element = 0;
  	$pager_page_array[$element] = $_GET['page'] ? $_GET['page'] : '';
	
	$per_page = variable_get('flickrhood_photos_per_page', 20);
	
	//request set photos
	$pfo = _flickrhood_get_pfo();
	$set_response = $pfo->photosets_getPhotos($set_arr['id']);
	
	if (!$set_response) {
		drupal_set_message('Problem retrieving user\'s set photos', 'error');
		return FALSE;
	}
	elseif (!array_key_exists('photo', $set_response) || empty($set_response['photo'])) {
	    drupal_set_message('This photoset is empty');
	    return '';
	}
	
	//set pager information we just acquired
	$pager_total_items[$element] = sizeof($set_response['photo']);
	$pager_total[$element] = ceil($pager_total_items[$element]/$per_page);
	
	//get starting position
	$page_num = is_numeric($_GET['page']) ? $_GET['page'] : 0;
	if (sizeof($set_response['photo']) > ($page_num * $per_page)) {
		$arr_pos = $page_num * $per_page;
	}
	else {
		$arr_pos = 0;
	}
	
	//extract photos relevant to this page
	$photo_info = array_slice($set_response['photo'], $arr_pos, $per_page);
	
	return theme('flickrhood_user_set', $uid, $per_page, $photo_info, $set_arr);
	
}

function flickrhood_viewuserphotosbytags($uid, $tags) {

	//set some required pager stuff
	drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);
	global $pager_page_array, $pager_total,$pager_total_items;
	//set this to something else if you want multiple pagers
	$element = 0;
  	$pager_page_array[$element] = $_GET['page'] ? $_GET['page'] : '';
	
	$per_page = variable_get('flickrhood_photos_per_page', 20);
	
	//parse the tags out into an array
	$tag_arr = explode(',', $tags);

	$fp_arr = _flickrhood_public_photos($uid, $tag_arr, $pager_page_array[$element]+1, $per_page);
	
	if ($fp_arr===FALSE) {
	    return '';
	}
	
	//set pager information we just acquired
	$pager_total[$element] = $fp_arr['pages'];
	$pager_total_items[$element] = $fp_arr['total'];
	
	return theme('flickrhood_user_photos', $uid, $fp_arr);
  	
}

function theme_flickrhood_user_photos ($uid, $fp_arr) {
	
	drupal_add_css(drupal_get_path('module', 'flickrhood') . '/flickrhood.css');
	
	$output = 
    theme('pager', NULL, variable_get('flickrhood_photos_per_page', $fp_arr['perpage']));
	
	$output .= "<ul id=\"flickrhood_photolist\">\n";
    
    if (array_key_exists('photo', $fp_arr)) {
	    foreach ($fp_arr['photo'] as $photo) {
	    	$imgpage_url = 'http://www.flickr.com/photos/' . $photo['owner'] . '/' . $photo['id'];
	    	$img_url = 'http://static.flickr.com/' . $photo['server'] . '/' . $photo['id'] . '_' . $photo['secret'] . '_t.jpg';
	    	
	        $output .= '<li>';
	        $output .= l("<img src=\"$img_url\">", $imgpage_url, array(), NULL, NULL, FALSE, TRUE);
	        $output .= '</li>';
	        $output .= "\n";
	
	    }
    }

    $output .= '</ul>' . "\n";
    
    $output .= 
    theme('pager', NULL, variable_get('flickrhood_photos_per_page', $fp_arr['perpage']));
    
    return $output;
}

function theme_flickrhood_user_set($uid, $per_page, $photo_info, $set_arr) {
	
	drupal_add_css(drupal_get_path('module', 'flickrhood') . '/flickrhood.css');
	
	$fid = $set_arr['owner'];
		
	$output = 
    theme('pager', NULL, variable_get('flickrhood_photos_per_page', $per_page));
	
	$output .= "<ul id=\"flickrhood_photoset\">\n";
    
	foreach ($photo_info as $photo) {
		$imgpage_url = 'http://www.flickr.com/photos/' . $fid . '/' . $photo['id'];
	    $img_url = 'http://static.flickr.com/' . $photo['server'] . '/' . $photo['id'] . '_' . $photo['secret'] . '_t.jpg';
	    
	    $output .= '<li>';
	    $output .= l("<img src=\"$img_url\">", $imgpage_url, array(), NULL, NULL, FALSE, TRUE);
	    $output .= '</li>';
	    $output .= "\n";
	
	 }
    

    $output .= '</ul>' . "\n";
    
    $output .= 
    theme('pager', NULL, variable_get('flickrhood_photos_per_page', $per_page));
    
    return $output;
	
}

function theme_flickrhood_user_tagcloud($uid, $fid, $tag_arr) {
	
	drupal_add_css(drupal_get_path('module', 'flickrhood') . '/flickrhood.css');
	
	$max_occur = max($tag_arr);
	$min_occur = max($tag_arr);
	
	// Font size specified in em
	$max_font = 3.5;
	$min_font = 0.7;
	
	$output = '<ul id="flickrhood_tagcloud">' . "\n";
	foreach ($tag_arr as $tagname => $occurences) {
		
		$font_size = round(($max_font-$min_font)*sqrt($occurences/$max_occur)+$min_font, 1);
		
		$output .= '<li style="font-size: '.$font_size.'em">' . "\n";
		$output .= l($tagname, 'flickrhood/' . $uid . '/tags/' . $tagname) . "\n";
		$output .= "</li>\n";
		
	}
	$output .= '</ul>' . "\n";
	
	return $output;

}

function theme_flickrhood_user_taglist($uid, $fid, $tag_arr) {
	
	drupal_add_css(drupal_get_path('module', 'flickrhood') . '/flickrhood.css');
	
	$output = '<ul id="flickrhood_taglist">' . "\n";
	$first_char = '';
	foreach ($tag_arr as $tag) {
		if ($first_char != substr($tag,0,1)) {
			$new_item = TRUE;
		}
		else {
			$new_item = FALSE;
		}
		
		if ($first_char!='' && $new_item) {
			$output .= "</ul></li>\n";
		}
		if ($new_item) {
			$first_char = substr($tag,0,1);
			$output .= "<li><ul>\n";
		}
		
		$output .= '<li>' . "\n";
		$output .= l($tag, 'flickrhood/' . $uid . '/tags/' . $tag) . "\n";
		$output .= '</li>' . "\n";
	}
	if ($first_char!='') {
		$output .= "</ul></li>\n";
	}
	$output .= "</ul>\n";
	
	return $output;
}

function theme_flickrhood_user_setlist($uid, $fid, $set_arr) {
	
	drupal_add_css(drupal_get_path('module', 'flickrhood') . '/flickrhood.css');
	
	$output = '<ul id="flickrhood_setlist">' . "\n";
	
	foreach ($set_arr as $set) {
		$output .= '<li>' . "\n";
		
		//write photo
		if (isset($set['primary'])) {
			$thumb = '<img src="http://static.flickr.com/' . $set['server'] .
					'/' . $set['primary'] . '_' . $set['secret'] .
					'_t.jpg' . '">';
			$output .= l($thumb, "flickrhood/$uid/sets/" . $set['id'] ,array(),NULL,NULL,FALSE,TRUE);
			
		}
		
		if (!empty($set['title'])) {
			$output .= '<span class="flickrhood_settitle">' . "\n";
			$output .= l($set['title'], "flickrhood/$uid/sets/" . $set['id']);
			$output .= '</span>' . "\n";
		}
		
		if (!empty($set['description'])) {
			$output .= '<span class="flickrhood_setdescription">' . "\n";
			$output .= $set['description'];
			$output .= '</span>' . "\n";
		}
		
		$output .= '<div class="clearleft"></div>' . "\n";
		$output .= '</li>' . "\n";
			
	}
	
	$output .= "</ul>\n";
	
	return $output;
}

function _flickrhood_get_fid($uid) {
	//get users fid
	$fid = db_result(db_query('SELECT fid FROM {flickrhood_users} where uid=%d', $uid));
	if (!$fid) {
		return FALSE;
	}
	else {
		return $fid;
	}
}

function _flickrhood_get_setinfo($setid) {
	
	$pfo = _flickrhood_get_pfo();
	$set_response = $pfo->photosets_getInfo($setid);
	
	//check for error
	if (!$set_response) {
		//TODO watchdog
		return FALSE;
	}
	
	//TODO set does not exist
	
	return $set_response;	
}

//returns an array with the flickr user info
//array['uid'] = unique flickr user id
//array['username'] = flickr user name
// TODO watchdog errors with more specific error message and better user error messages
function _flickrhood_find_user($f_user_array) {
	
	$pfo = _flickrhood_get_pfo();
	
	//booleans to keep track of success
	$mail_success = FALSE;
	$username_success = FALSE;
	
	//try to find by email
	if (array_key_exists('mail', $f_user_array) && !empty($f_user_array['mail'])) {
		
		$mail_response = $pfo->people_findByEmail($f_user_array['mail']);

		if ($mail_response!==FALSE) {
			$mail_success = TRUE;
	  		$user_info = array(
				'uid' => $mail_response['nsid'],
				'username' => $mail_response['username'],
			);
		}
	}
	
	//try to find by username
	if (array_key_exists('username', $f_user_array) && !empty($f_user_array['username']) && !$mail_success) {
		
		$username_response = $pfo->people_findByUsername($f_user_array['username']);
		
		if ($username_response!==FALSE) {
			$username_success = TRUE;
		  	$user_info = array(
		  		'uid' => $username_response['nsid'],
				'username' => $username_response['username'],
			);
		}
		
	}
	
	if ($mail_success || $username_success) {
	    return $user_info;
	}
	else {
	    return FALSE;
	}
		
}		

/* Implementation of hook_settings */
function flickrhood_admin_settings() {
  $form['flickrhood_apik'] = array('#type' => 'textfield', '#title' => t('Flickr API Key'), 
			       '#size' => 60, '#maxlength' => 128, '#required' => TRUE, 
			       '#default_value' => variable_get('flickrhood_apik',''));
  $form['flickrhood_photos_per_page'] = array('#type' => 'textfield', '#title' => t('Photos per page'), 
			       '#size' => 3, '#maxlength' => 3, '#required' => TRUE, 
			       '#default_value' => variable_get('flickrhood_photos_per_page','20'));
  $form['flickrhood_tags_in_cloud'] = array('#type' => 'textfield', '#title' => t('Tags in tag cloud page'), 
			       '#size' => 3, '#maxlength' => 3, '#required' => TRUE, 
			       '#default_value' => variable_get('flickrhood_tags_in_cloud','150'));
  $form['flickrhood_updateinterval'] = array('#type' => 'textfield', '#title' => t('Update interval in minutes'), 
			       '#size' => 5, '#maxlength' => 5, '#required' => TRUE,
			       '#description' => 'User settings will update when cron.php is run if the last update was at least this value of time apart from the current time',
			       '#default_value' => variable_get('flickrhood_updateinterval',0));

  return system_settings_form($form);
}

/* Implementation of hook_cron */
function flickrhood_cron() {
	//interval in seconds
	$interval = time() - variable_get('flickrhood_lastupdate', 0);
	//in minutes
	$interval = $interval / 60;
	
	if ($interval > variable_get('flickrhood_updateinterval', 0)) {
		//update all users
		variable_set('flickrhood_lastupdate', time());
		
		$result = db_query('SELECT uid FROM {flickrhood_users}');
		
		while ($user_obj = db_fetch_object($result)) {
			$uid = $user_obj->uid;
			_flickrhood_update_user_tags($uid);
		}
	}
}

// Help. XXX: TODO: Fill it in.
function flickrhood_help($section) {
  switch ($section) {
  case 'admin/modules#description':
    return t('Allows a community to integrate their Flickr photos into Drupal');
  }
}

/* Returns an array of the users most recent photos */
function _flickrhood_update_user_tags($uid) {
	
	//get users fid
	$fid = _flickrhood_get_fid($uid);
	
	// TODO: watchdog
	if ($fid === FALSE) {
	    return FALSE;
	}
	
	$tag_array = array();
	$existing_tags = array();
	
	//fetch users cached tags
	$result = db_query('SELECT t.tag
					  FROM {flickrhood_tags} as t, {flickrhood_users_tags} as ut
					  WHERE t.tag_id=ut.tag_id AND ut.uid=%d', $uid);
	
	while ($row = db_fetch_object($result)) {
		$existing_tags[] = $row->tag;
	}
	
	//fetch tags from flickr
	$pfo = _flickrhood_get_pfo();
	$taglist_response = $pfo->tags_getListUser($fid);
	
	if (!$taglist_response) {
		return FALSE;
	}
	
	$delete_queue = array_diff($existing_tags, $taglist_response);
	$insert_queue = array_diff($taglist_response, $existing_tags);

  	foreach ($insert_queue as $tag) {
		//find out if it already exists
		$result = db_query('SELECT tag_id FROM {flickrhood_tags}
				 WHERE tag=\'%s\'', $tag);
		
		if (db_num_rows($result) > 0) {
			//if it does, do save the id
			$tag_id = db_result($result);
		}
		else {
			//we need to insert the new tag and save the id
			$tag_id = db_next_id('flickrhood_tags');
			db_query('INSERT INTO {flickrhood_tags} (tag_id, tag)
					 VALUES (%d, \'%s\')', $tag_id, $tag);
		}
		//add users tag
		db_query('INSERT INTO {flickrhood_users_tags} (uid, tag_id)
				 VALUES (%d, %d)', $uid, $tag_id);
  	}
  	
  	
  	
  	//delete tags that are no longer used by the user
  	foreach ($delete_queue as $dead_tag) {
  		$dead_tag_id = db_result(db_query('SELECT tag_id
  								   		FROM {flickrhood_tags}
  								    		WHERE tag=\'%s\'', $dead_tag));
  		db_query('DELETE
  				 FROM {flickrhood_users_tags}
  				 WHERE uid=%d AND tag_id=%d', $uid, $dead_tag_id);
  	}
}

/* 
 * _flickrhood_public_photos will return an array of information about the photos you requested.
 * Photo array returned is generated by a photos_search call.
 */
 
function _flickrhood_public_photos($uid, $tags=array(), $page=1, $per_page=NULL) {
	//get users fid
	$fid = _flickrhood_get_fid($uid);
	
	if ($fid===FALSE) {
		drupal_set_message('Requested user has not entered a valid Flickr username or email', 'error');
		// TODO watchdog error here
		return FALSE;
	}
	
	//set $per_page
	if ($per_page==NULL) {
		$per_page = variable_get('flickrhood_photos_per_page', 20);
	}
	
	$params = array ('user_id' => $fid,
					'per_page' => $per_page,
					'page' => $page);
	
	if (!empty($tags)) {
		$params['tags'] = implode(',', $tags);
		$params['tag_mode'] = 'all';
	}
	
	$pfo = _flickrhood_get_pfo();
	$search_response = $pfo->photos_search($params);
	
	
	if (!$search_response) {
		drupal_set_message('Problem retrieving user\'s photos', 'error');
		return FALSE;
	}
  	
  	if (!array_key_exists('photo', $search_response) || empty($search_response['photo']) ) {
	  	drupal_set_message('No photos found');
	  	return FALSE;
  	}
  	
  	return $search_response;
}

/* get a phpFlickr object */
function _flickrhood_get_pfo($secret = NULL, $die_on_error = false, $api_key = NULL) {
	include_once('lib/phpFlickr/phpFlickr.php');
	
	if ($api_key == NULL) {
	    $api_key = variable_get('flickrhood_apik', '');
	}
	
	return new phpFlickr($api_key, $secret, $die_on_error);
}

?>

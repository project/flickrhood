
This is a modified (Drupalized) version of the phpFlickr Class 2.0.0.
It has been relicensed under the GPL version 2 as part of this project.


The text below was taken from the phpFlickr README.txt:

phpFlickr Class 2.0.0
Written by Dan Coulter (dancoulter@users.sourceforge.net)
Project Homepage: http://www.phpflickr.com/
Sourceforge Project Page: http://www.sourceforge.net/projects/phpflickr/
For more information about the class and upcoming tools and applications using it,
visit http://www.phpflickr.com/ or http://www.sourceforge.net/projects/phpflickr/